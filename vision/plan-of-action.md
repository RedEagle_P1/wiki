# Plan of action

## Our movement

We are a movement that supports concrete plans of action, put forward by [our members](#user-content-fn-1)[^1], that will help us implement the values of the Charter.&#x20;

## Active plans

### (1) Open Collective

<details>

<summary>About [P1-OC]</summary>

#### The problem

Corporations have an obligation to focus on generating profit for their investors rather than serving their customers.&#x20;

This often leads to technology which [harvests people’s data ](https://www.p1dc.org/ourvision/behind-the-vision/behind-the-charter#why-privacy-matters)and disregards their rights.

Closed meetings and opaque decisions are made over platforms without any feedback from the users themselves.

#### Our solution

We have invented a new form of social organization called the Open Collective. The entire incentive structure of the Open Collective has been designed to enable the creation of virtual worlds which can compete with those made by a traditional company while being deeply focused on protecting the rights and freedoms of those who inhabit the Metaverse.

**The competitive advantage**

**Labor capital**

Rather than depending on investment capital, the Open Collective sources labor capital from its members. Members are volunteers who can come and go as they please, without minimum hours of contribution. The secret of an Open Collective is to synthesize these microcontributions into working software, a huge challenge.

Although most members contribute for the sake of the cause, an Open Collective divides 100% of net profit made from the products its members create to members who sign up for the Revenue Royalty Program “RRP”.

Nothing written on this page should be constituted as an obligation to any volunteers or members. Rather, the RRP itself defines the actual commitments and guarantees made by the Open Collective.

**Royalty program**

The RRP is designed to sustain those who seek to contribute regularly to the Open Collective. The program requires more commitment from those who sign up but those who do are partially sustained by the 80% that choose not to.

The fact that contributions are voluntary means that decisions which don’t reach moral consensus will lead to an immediate drop off of volunteer support.

Moreover, those that do sign up, donate back a completely voluntary portion of their income to the Open Collective in order to enable the Open Collective to acquire a marketing budget and deal with legal overhead. The ability to change that donation percentage is critical to our checks and balances.

Since 100% of profit is sent back to members, without that donation percentage the collective cannot exist.

Our experience so far is that these two checks on power make it nearly impossible for an Open Collective to make unethical, closed decisions.

Because the motivation of 80% of the contributors is voluntary and related to the moral cause of the collective, immoral decisions cause the collective to cease to exist. And because the collective depends on the voluntary donations percentage of those it enriches, it ceases to exist unless it serves its members interests.

**The power of royalties**

With so many restrictions, how does an Open Collective out-compete a company? The ability to work with anyone, anywhere at any time is critical to its success.

Since royalties are a globally understood mechanism for rewarding creators, the RRP enables us to synthesize the microcontributions from people around the world to build something much bigger than any contributor could build alone.

**Why not go Open Source?**

[Learn why >>](https://www.p1dc.org/ourvision/behind-the-vision/behind-the-plan-of-action#why-not-go-open-source)

#### Progress

So far, this has been our most successful experiment. Having won in 6, $30,000 game competitions and having attracted 300 active game developers as members as of 2023, the Open Collective is getting closer to becoming a proven model.

#### How you can help

You can join us at [http://p1om.com/tour](http://p1om.com/tour)

</details>

### (2) \[P1-AI]

<details>

<summary>About [P1-AI]</summary>

### The problem:

**Personalized Advertising**

Advertisements do their best when they deliver the most **relevant ads** to the most relevant people.

After all, there's **no use** selling fat loss programs to the **skinny**.

The desire for advertising revenue, will drive virtual world creators to **spy on you**, on your most **intimate moments**.

This could set the precedent for a world in which **people no longer feel free** to express themselves.

#### Our solution

\[P1-AI] is a brand new **Open Collective** which seeks to build a new type of **advertising platform** which does **not seek** to harvest your **private data**.

Together we're building **a new foundation** for the free, interactive, internet that puts **people at the center**.

The goal is to connect **data scientists** with **gamers** by allowing data scientists to leverage **micro-contributions** by gamers in order to annotate images.

This is not too different from **captchas** but rather than the **data** going just to Google **anyone** can plug their queries into the system.

#### Progress

This project is in the inception phase. Currently we are working on the legal foundation and a novel Open Collective structure designed to solve this specific problem.

#### How you can help

1. Donate: [http://p1om.com/contribute](http://p1om.com/contribute)
2. If you are a lawyer, get in touch on [Discord ](https://discord.gg/2sVsZ6NC6B)(preferred) or email admin\[at]p1om.com

</details>

{% hint style="info" %}
_Suggest_ [_an edit here_](https://www.p1dc.org/help/)_._
{% endhint %}

[^1]: [Become a member >>](https://p1om.com/join)
