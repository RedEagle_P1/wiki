# Behind the Charter

We believe that all people are born with a myriad of rights and freedoms, but that those rights must sometimes be contravened in order to create functioning social organization.

The charter aims to highlight the rights and freedoms that must be by no means contravened for any reason.

## Why privacy matters

The founding fathers of the United States understood that data was essential to the balance of power between a government and its people:

4th amendment of the US constitution

> “The right of the people to be secure in their persons, houses, papers, and effects, against unreasonable searches and seizures, shall not be violated.”

A government that can’t understand what is going on with it’s citizens can’t find arbitrary reasons to persecute them but a government with unlimited data quickly becomes a government with unlimited power.

This is exactly what's happening today in China. People have volunteered their data in exchange for the convenience of technology only to find it used against them in creative and twisted ways.

A social credit score has been established to incentivize behavior the Chinese Communist party appreciates. Speak carelessly and you'll find yourself locked out of public transportation and the banking system. Even eating the wrong foods could lower your social credit score.

## Session-tracking vs profile-tracking

For any interface to work appropriately, it needs to know, understand you and track you. However, when that system creates or links your actions with a greater profile on you, your freedoms are under threat.

## Personalized advertising

Advertisements do their best when they deliver the most **relevant ads** to the most relevant people.

After all, there's **no use** selling fat loss programs to the **skinny**.

The desire for advertising revenue, will drive virtual world creators to **spy on you**, on your most **intimate moments**.

This could set the precedent for a world in which **people no longer feel free** to express themselves.

{% hint style="info" %}
_Suggest_ [_an edit here_](https://www.p1dc.org/help/)_._
{% endhint %}
