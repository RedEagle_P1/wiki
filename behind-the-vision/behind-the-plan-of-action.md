# Behind the plan of action

## Why not go Open Source?

Our values are deeply aligned with open source but after a thorough examination we decided to switch to becoming an Open Collective.

To understand why you have to understand how we view the Metaverse.

We believe that the network effect will cause the Metaverse to coalesce to a few primary virtual worlds in the social realm. We believe that the same forces that drove centralization in Web2 will continue to drive centralization in the Metaverse.

Let’s say we build an incredible Open Source social virtual world, what stops a large corporation, taking the technology, extending it and using their superior marketing budget to get more users?

After all, a social network is not greater than the people within it. If any one social virtual world gains dominance in the Metaverse, the network effect will cause the networks to reduce their relevance.

## What is the network effect?

A long time ago when the phone network was not interoperable, if you bought a phone on the AT\&T network, you could not speak to people on other networks. Namely, people would have to buy an AT\&T phone to speak to AT\&T customers. This made it naturally attractive for everyone to use AT\&T. This is the network effect, and it’s why social networks tend to coalesce and grow.

Interoperability is only going to be made more difficult due to the complexity of technology in the future. We believe interoperability is likely in the interest of everyone, but it’s also unlikely to take place because the financial incentive to create less interoperability is high.

Moreover, metaverse interoperability is exceedingly complicated.

[Learn more >>](https://www.p1dc.org/ourvision/articles/conclusions-from-a-decade-of-research#7.-item-interoperability-is-unlikely)

## The long history of failed open source alternative social networks

Throughout the rise of Facebook competitors arose espousing the values of privacy that would come through an open source alternative. Time and time again these open source networks failed despite widespread interest in the idea.

Diaspora is a good example.

We believe open source alternatives will continue to preform badly in the virtual world revolution for several key reasons:

### 1) The incentive problem

The primary incentive of an open source contributor is to extend existing software. This means that open source collectives are filled with engineering-minded people.

This makes them like cars missing a wheel. They often build the most incredible products but have terrible onboarding and no marketing.

Open collectives on the other hand are filled with a diverse range of people -- similar to modern companies giving them a serious competitive advantage.

What's wrong with an engineer-only team?

**1-1) Bubble bias**

Every particular group of people tends to be in their own bubble of coworkers and friends. From these people we get our personal ideas of “normal.”

It's easy to get into the tech “bubble” and to forget that the average person struggles to do more than send an email.

This study gives us a good look at how limited the average computer user really is: www.nngroup.com/articles/computer-skill-levels

26% couldn’t even use a computer. Only 5% could find out: “what percentage of the emails sent by John Smith last month were about sustainability.”

**1-2) Working with people**

Engineers tend to love to work with computers rather than people. Computers are much easier to understand in their component parts than a human being who has free will. Humans are unpredictable and their behavior is so diverse it's very hard to predict. Understanding how groups of humans work together at scale is typically niche with little overlap to engineering.

This skill is possibly the most important skill to make a functioning social network.

**1-3) Marketing, UX etc.**

Great social networks are filled with other people to interact with. If you don't have people, you don't have a social network. To get a good number of people into a virtual social network you often need to overcome massive onboarding challenges as ordinary people struggle to understand their new virtual existence.

User experience people are deeply passionate about the subject but have no incentive to contribute to an open source project.

More than anything, engineers love to tinker with the world around them. Therefore, engineers tend to create highly modifiable products with lots of options. This is terrible for onboarding new users but it's extremely convenient for the contributors of an open source project who tend to be highly technologically savvy.

In open source projects simplification is downvoted 9 out of 10 times because simplification fails to address the needs of those contributing to the project.

### 2) Embrace, extend, extinguish

Even if a team of incredible engineers were to build an open source social network the reality is that the average consumer doesn't care about their privacy. We technologists hold privacy very dear but as the average user struggles to even make use of technology, they care more about what's usable than what's private.

It's quite easy for a corporation to raise capital and to use superior marketing to popularize a social network built by open source collectives.

## Our solution

Open Collectives were built to solve this problem. By having revenue generation be a part of the model we create an incentive for everyone to onboard as many users as possible. Moreover, we have already proven the ability to attract a wide diversity of talent well outside of the typical engineering collective.

We do all of this without sacrificing the open, collaborative and people-1st nature of open source.

{% hint style="info" %}
_Suggest_ [_an edit here_](https://www.p1dc.org/help/)_._
{% endhint %}
