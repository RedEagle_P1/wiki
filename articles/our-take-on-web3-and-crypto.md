# Our take on Web3 and Crypto

As a movement, we are deeply aligned with the values behind Web3, but we feel that undermining the current court system and government is not the best means to the goal of creating a freer Metaverse.

Our vision doesn't necessarily involve the web or decentralized consensus either. We believe that to compete with privacy-detrimental systems, we must outperform them and we are agnostic to the means of achieving efficiency.

We are open to plans of action from our members which fulfill the goals of our charter without sacrificing usability or performance for the end user.

We should also mention, we don’t sell a token or encourage plans which do in order to preserve the purity of the movement.

{% hint style="info" %}
_Suggest_ [_an edit here_](https://www.p1dc.org/help/)_._
{% endhint %}
