---
description: Written by Samuel Martin
---

# Conclusions from a decade of research

## Preface:

The Metaverse is a hypothetical version of the internet in which 3D avatars navigate a 3D space. Companies envision this world to be one where most people will live most of the most meaningful parts of their life in the future.

## 1. The Metaverse is not possible with mouse and keyboard

As Metaverse enthusiasts, it's important to remember that we are likely the top 0.1% of computer users. Most people are confused by technology, overwhelmed by options and worried if they push the wrong button they will destroy something.

Chrome added a copy and paste feature to the menu because people couldn't discover how to copy and paste.

Since the Metaverse simulates the countless possibilities of physical life, the complexity of the user interface gets out of hand quickly.

I remember testing out Second Life and accidentally taking off my pants and being unable to put them back on for 20 minutes.

Clearly, mouse and keyboard would not be a sufficient way of interacting with the metaverse.

## 2. No significant social organization arises out of putting people in a virtual space

18 years ago, Second Life came out and the hype around the Metaverse future was just as real back then as it is today. You can visit the empty government buildings, brand showcases and cities to get a taste of the forgotten glory.

Time and time again people have been put together in a virtual space, and we still went back to physical life for real connections.

We have to ask ourselves, why did we leave virtual worlds and go back to physical life?

## 3. The nature of virtual spaces leads to shallow relationships

The great benefits of any relationship are usually found in the most challenging operations of cooperation, marriage is a great example. Dating is exciting but sharing a life with someone is deeply sacrificial and yet, the benefits of marriage are enormous.

In digital worlds we scarcely invest as heavily in relationships as in physical life. When we can go anywhere, be with anyone it's easy to constantly look for greener grass.

This problem is accentuated by the ability to change your name/avatar at will. When people can burn others and simply change their identity to escape consequences, they do. Investing in a real reputation and a single identity is the first step to solving this issue.

## 4. The Metaverse will arise from a gaming world

In digital worlds, we find ourselves mixed in with people from different cultures who have different values and interests. By contrast, in physical life we are naturally segmented with people who are more likely to share our values and ideas.

Gaming has the power to break ice between people and therefore has intrinsic utility to the Metaverse.

By using gamification we can organize and connect people of similar interests and values in a virtual space.

Not only that, gaming has the power to get people to relax. Human beings don’t develop meaningful friendships when forced into it. This is what social games and “virtual life” games often get wrong. Actual gameplay builds the context we need to relax and have a conversation to find people we are compatible with.

## 5. Reciprocating facial expressions are necessary for cooperative human behavior

If you've ever lifted a baby and smiled at it, you know that facial expressions are deeply embedded in how we communicate. When we hurt someone's feelings, their facial expressions changes and, as we reciprocate their facial expressions, our body produces the same emotions as we make that individual feel. This acts as a natural tempering of our behaviour that we don't get when we communicate over the internet.

## 6. Customization is the enemy of usability

It's fascinating to bump into so many people who share the idea that no corporation should control the future of human interaction. However, it's laughable that we're using the same ill-advised ideas that allowed Facebook, Google and apps to dominate when it comes to this next chapter.

One of the reasons that Facebook overtook MySpace was that MySpace allowed people to customize their profile infinitely. This led to a myriad of different user interfaces, confusing navigation and a poor user experience.

Just imagine a myriad of virtual worlds all with their own user interface, controls and standards. The experience of traveling from world to world would be like being born all over again and having to learn how to read and write.

Therefore, the virtual world must have a single user experience to reach mass adoption.

## 7. Item interoperability is unlikely

I keep hearing from people who believe that in the future, NFT items will be taken between games and virtual worlds. I sometimes wonder if any of these people have actually designed a video game.

I'm in the process of making one now and the thing you discover very quickly is that everything in a video game is intentional.

For example; if you have a character in your world, all the items in your world fit the style, height and width of that character.

Let’s say you introduced a car from one game into another. That car could:

(1) Not fit the style of the game (2) Break the game balance, giving the player an advantage (3) Not fit how light reflects in the game (4) Have a different control interface (5) Be too wide for the road (6) Have the steering wheel on the wrong side (7) Create uncalculated physical results, resulting in a car that sends other cars to space

And the list goes on.

## 8. The network effect would eventually push everyone to a single virtual world

A long time ago, there was a single company that dominated the telephone market. To call anybody who was the customer of the company you needed to have a phone that was with that same company, phones were not interoperable. Today, if you want to contact somebody on Facebook, you need Facebook to do so. This is how social networks defend their intellectual property.

Meta executive Jason Rubin:

“The first metaverse that gains real traction is likely to the be the last,” Rubin wrote. “We must act first, and go big, or we risk being one of those wannabes.” - CNBC

The network effect is a scary prospect because it means that there will likely be only one Metaverse to rule them all in the end.

## 9. AR and not VR will make the Metaverse mainstream

Virtual reality headsets have been pitched as a game changer for years now. However, very few people feel comfortable obstructing the entirety of their vision to enter our “geek world''. I think we have to accept that even though we love the medium it's unlikely to have mass adoption anytime soon.

Augmented reality will allow people to continue their lives as they begin to adopt the new virtual way of doing things.

CEO of Apple on AR:

“...one of these very few profound technologies that we will look back on one day and ask, ‘How did we live our lives without it?’”

## 10. Distributed governance is necessary to limit censorship

If any one platform is responsible for the content on the Metaverse, it will be torn apart by that responsibility. Human beings have never agreed on morals and values and to one person black is white and white is black. We are slowly coming out of an incredible era of tolerance which is abnormal for human beings.

We have spent thousands of years of our history destroying each other over needless inconsequential arguments about small religious differences etc.

As power moves to the virtual space instead of the physical, everyone is going to be pushing the power broker, the Metaverse platform, to impose their sense of morality on others.

Eventually these arguments will inevitably tear apart the Metaverse unless responsibility for individual nodes is decentralized.

## 11. The Metaverse has the power to become a dystopian nightmare

As the network effect brings everyone to a single Metaverse, the potential for the abuse of power will be astronomical. The Metaverse will understand where you are, who you are with and even what you look at.

AI will be able to process that data to better understand you than you yourself and data is the first step toward oppression.

Utilizing all this data, powerful individuals will have the ability to suppress dissent, identify people of contrarian opinion etc.

Moreover, if we begin to fool ourselves that we can agree on morals and values, those who disagree with the mainstream will live in constant fear of losing their friends, job and opportunities if they are banned from the Metaverse.

## 12. Decentralization will not aide data protection

The web as we know it today is a well distributed system, however, it provides no real protection for our data. Every website constantly scrambles after our data and what companies know about us is incalculable. Fortunately, the amount of data makes driving conclusions difficult right now but in the future AI will solve that problem.

If decentralization isn't protecting us, there is no reason it will protect us in the Metaverse.

## 13. Censorship is necessary to create community

Every space in the physical world has some sort of rules to follow. A library asks us to be quiet, a classroom to be respectful and a workplace to be professional. In the same way creators of spaces must be empowered to moderate the spaces they watch over to create a sense of identity in the space which will help it facilitate connections.

Total freedom everywhere will just lead to chaos.

## 14. Open source may not be the answer

With the advantages of usability of having only one way of doing things in a virtual space, embrace, extend, extinguish takes on a whole new meaning.

It's too easy for those who would profit from our collaborative efforts to use those efforts to create a singular Metaverse which is more usable and then use the “import but don’t export” model to bring in the best of virtual worlds without allowing anything out of their world.

{% hint style="info" %}
_Suggest_ [_an edit here_](https://www.p1dc.org/help/)_._
{% endhint %}
