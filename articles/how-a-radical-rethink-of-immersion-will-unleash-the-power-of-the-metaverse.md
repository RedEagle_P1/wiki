# How a radical rethink of immersion will unleash the power of the Metaverse

Movies like the Matrix and Ready Player One filled us with wonder at the potential of immersive virtual worlds. However, they may have also given us a flawed view of how the Metaverse will actually develop.

The challenge of building a persistent, immersive virtual experience that is practically beneficial for end-users will be the supreme achievement of the Metaverse movement, but to get there we need to understand the major obstacles that stand in our way.

## The obstacle of hardware

### The miniaturization of technology

We have a ways to go to miniaturize the CPU, battery and other systems sufficiently for them to fit comfortably on our head. It will take around 5 to 7 years for augmented reality (AR) glasses to really compete with smart phones.

### Facial expressions

Reciprocating facial expressions are critical for human empathy. With so much of human communication being nonverbal, bringing these expressions across in a human way is critical to the rise of the Metaverse.

We all remember the first time we smiled at a baby and it smiled back. The power of facial expressions is that they allow us to transfer emotions between people. As one person smiles, so does another, but the power of a smile is that it releases endorphins throughout your body.

Without this reciprocation human behavior is unrestrained as we fail to feel the impact we have on others with our words.

## The obstacle of software

### Duplicating physical life

Getting immersion right is a difficult task and companies often make the mistake of directly duplicating physical life.

Most Metaverse enthusiasts envision cities much like our own. However, the modern city is built around the challenges of overcoming vast distances.

Virtual worlds have no reason to have cars, parking lots, or vast distances. Each virtual world should be built with a particular use case in mind. We should be thinking about why people visit our world and how we must design our world accordingly.

### Functioning use cases

We’ve had immersive virtual worlds for more than two decades now and during that time almost every application of a virtual world has been tested.

Of the applications tried, these were the only ones found to be viable:

Dating — virtual dating is a niche use case that actually works for a percentage of the population.

Gaming — probably representing 97% of the use of virtual worlds, gaming is by far the largest used case for immersive virtual experiences. We will include gambling and immersive travel in this niche.

Social — virtual worlds that are purely social are a small niche of a much greater virtual community. By-and-large social activities have to be paired with some other activity.

Fitness — we recently discovered that VR set ups are great for getting exercise.

Science, therapy, education, and other purported use cases have mostly been overhyped and quite unsuccessful so far.

There are other use cases but they are incredibly niche.

### Discovering new use cases

When discovering new use cases, we have to consider not only the layout of our virtual world, but if total immersion is really the best answer.

Rather than building an entire city to duplicate physical life, we must look at one particular vertical and decide both the level of immersion necessary, and the features necessary to test if the use case is viable.

#### Trying to develop a game in the Metaverse

For the sake of experimentation, I took a game development team of 100 people into Altspace VR to develop a game.

Obviously, we could not run our game development software in Altspace, but we had all of our live meetings there.

Here's what we noticed:

**Impracticality of spatial audio**

It was easy in Discord to move from channel to channel to have private conversations, but in Altspace, you never knew how far you needed to move in order to be private.

**Immersion wasn't beneficial**

Any benefits we would've had by being together in a physical space weren't duplicated sufficiently enough to warrant the use of an immersive virtual space.

**Fractional immersion as a way of life**

In our Open Collective Game Studio everybody is highly motivated to get things done, and therefore switching to a different task during a live meeting is perfectly acceptable. If we don't feel what is being said is practical we mute and focus on something else.

Whereas you might be able to do this in a physical space by pulling out your phone, in a VR headset we were unable to switch applications and do something else.

Moreover, because everyone was staring at us if we did something else, and our hands would go limp when we did, it was quite awkward to do.

**What we learned**

Even though the experience of being in a virtual space was novel at first, it failed to provide real value for our particular use case.

The interesting thing is that we found ourselves asking the question if a physical space would benefit us at all. In conclusion, we have become so adept to social organization in a digital space that we concluded a physical space would also be a hinderance. Virtual worlds were poorly emulating something we didn't need much of in the first place.

Most people are not going to be as adept to using digital spaces as to make physical spaces irrelevant, but it brings up an interesting challenge to the supremacy of virtual worlds, the current 2D internet.

### Fractional immersion

I believe that if we are to discover new use cases, we should consider fractional immersion and augmented reality as critical.

Rather than shooting for the ideal of the Matrix for now we should focus on niche verticals that we can prove to be functional.

Immersing people fully without regard to their desire to do multiple things interchangeably is an error that puts virtual worlds in direct competition with the efficiency of 2D digital spaces.

Most people like to have many things at their fingertips and the ability to switch between them quickly. Whether it's chrome tabs or multiple different documents, the ability to switch from one thing to another quickly and efficiently improves our productivity.

Immersion should never come at the cost of that ability.

## How the Metaverse will rise

With all the above in mind, what are some likely paths toward the evolution of a persistent, immersive virtual world if there ever will be one?

Virtual worlds today are typically an impediment to the things people care most about when using computers, namely, ease-of-use, accessibility and speed.

By locking people into an immersive virtual experience, we stand in the way of the usefulness of virtual worlds.

### Hardware as the key

I firmly believe that the evolution of the hardware, and not the software, is the key to uncovering critical use cases.

We have to keep our eye closely on the options presented to us as the possibilities of the hardware become clear.

Any step taken before the hardware represents the possibilities as likely premature.

### Proverbial paths

Developing and networking multiple AR experiences which allow people to solve productivity problems in a niche vertical, will likely lead to a pseudo-Metaverse environment.

Depending on how networked those AR experiences are, they could lay the foundation for an extended reality (XR)-first generation that will natively hop into more immersive experiences.

## Conclusion

Immersive experiences must take into account that their competition is usually not the sense of presence of a physical space but the speed and useability of the current 2D digital environment.

If you take people into a totally immersive virtual space, you take them away from the power of their phone and the sense of presence in a physical space. That's a huge barrier for virtual spaces to overcome.

We are still many years off from the rise of a persistent virtual world and right now we should look at infrastructure use cases rather than end-user applications.

As AR hardware develops we should keep a close eye on it to see how we can serve users by creating efficient solutions to specific, verifiable needs.

{% hint style="info" %}
_Suggest_ [_an edit here_](https://www.p1dc.org/help/)_._
{% endhint %}
