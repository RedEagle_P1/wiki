# Table of contents

* [Vision](README.md)
  * [Charter](charter.md)
  * [Plan of action](vision/plan-of-action.md)
* [Behind the vision](behind-the-vision/README.md)
  * [Behind the Charter](behind-the-vision/behind-the-charter.md)
  * [Behind the plan of action](behind-the-vision/behind-the-plan-of-action.md)
* [Articles](articles/README.md)
  * [Conclusions from a decade of research](articles/conclusions-from-a-decade-of-research.md)
  * [Our take on Web3 and Crypto](articles/our-take-on-web3-and-crypto.md)
  * [How a radical rethink of immersion will unleash the power of the Metaverse](articles/how-a-radical-rethink-of-immersion-will-unleash-the-power-of-the-metaverse.md)
