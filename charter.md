# Charter

## Four essential rights

**Right to obscurity**

* The right not to be individually penalized for non-criminal acts done outside of a particular virtual world.&#x20;

**Right to migration**

* The right to switch from virtual world to virtual world without contractual hindrance.

**Right of privacy**

1. The right not to be tracked in an identifiable fashion, without expressed, written, and updated consent.
2. The right to separately agree to the tracking of political or religious beliefs.
3. The right to take down, or disassociate from, content published by oneself.

**Right of creation**

* The right of the creator of a virtual world to create the rules by which that virtual world will operate, **notwithstanding the first three rights.**&#x20;

## Why these rights are essential

* Every virtual world creator has the right to manage their own virtual world as they wish. However, if a virtual world becomes **essential for human life**, that creator may create large-scale censorship for a large part of the human population.&#x20;
* As long as we have the power to move between virtual worlds, free of contractual hindrance, we can still connect with those we love even if we break the laws of a virtual world.&#x20;

## Further rights & freedoms

**Freedom of expression**

* The right to have and share ideas, out loud or in writing, and to publish those ideas without restriction.

**Freedom of ownership**

* The right not to be arbitrarily deprived of the value or ownership of money or property.

**Freedom of association**

* The right to be around whomever you wish.

**Right of personal responsibility**

* The right to be judged as an individual.

**Right to differing values**

* The right to have and share beliefs and values without restriction.

**Right to due process**

* The right to be informed of infractions. The right to appeal those infractions.

**The condition**

* None of these rights constitute a right to any individual’s or group's property or time.

### **The golden rule**

* No one must be excluded from the Metaverse as a whole, without criminal prosecution being brought against them in the physical world.&#x20;

## **The conclusion**

* We sit at a crossroads with one path leading us toward total domination, and another path to exceptional freedom.

{% hint style="info" %}
_Suggest_ [_an edit here_](https://www.p1dc.org/help/)_._
{% endhint %}
