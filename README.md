# Vision

### Our vision

Our vision is a new, _virtual_ world in which **human life** and **freedom** reigns.

#### The rise of virtual civilization

We believe that one day the **virtual world** will be as **real** to us as the **physical**.

This revolution begins when **augmented reality** allows us to interact with the **digital** world in the same way we interact with the **physical world**.

* Just imagine -- rather than searching your computer’s file system for that lost PDF to share it, you can pick a virtual one up off your desk and “hand” it to someone in **virtual space**.
* Just imagine -- rather than building a **school** or **workplace**, you can just teleport where you want to go and see the people you normally see in those spaces.

When we are able to get the same **sense of presence** in a virtual space as in a physical space, we will **stop building** physical spaces.

### What we’re fighting for

We exist to **make sure** that world is _**People-1st**_ \[P1].

#### What does _People-1st_ mean?

People-1st means a world in which:

* **Human life** and **freedom** are cherished
* You own your own **property** and do with it as you **wish**
* You are **judged** for your **actions** alone.

Details in our [Charter >>](https://www.p1dc.org/ourvision/vision/charter)

#### What happens if we **don’t** defend our freedom?

In this virtual future, we will find ourselves at the complete mercy of virtual world platforms.

They will:

* Ban us without recourse
* Exploit us for profit

* Just imagine -- being **separated** by the sole network needed to connect with **family** and friends
* Just imagine -- being arbitrarily **separated** from your **possessions**
* Just imagine -- a world in which you are **afraid** to be **yourself**.\


Social media giants already **ban** their members on a regular basis **without even stating the reason**. For most, these platforms are not essential for life or income but **one day virtual worlds will be the reality** most call home.

We must fight for our **sovereignty** in that **reality** or it will be too late.

#### What happens if we **do** fight for freedom?

The disruption caused by this **change of realities** gives us a unique opportunity to reshape civilization’s underlying structures to put people at the center like never before.

Together we can breathe life into a new world in which:

* You can be **with anyone** anywhere
* You can **speak your mind** without fear
* Your **life**, **property** and **freedom** are cherished above all.

{% hint style="info" %}
_Suggest_ [_an edit here_](https://www.p1dc.org/help/)_._
{% endhint %}
